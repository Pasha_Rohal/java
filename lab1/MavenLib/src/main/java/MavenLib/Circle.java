package MavenLib;

public class Circle {
    private double radius;
    private double x;
    private double y;
    private static final double PI = 3.14;

    public Circle(double radius, double x, double y) {
        this.radius = radius;
        this.x = x;
        this.y = y;
    }

    public double area() {
        return PI * Math.pow(radius, 2);
    }


}
