package dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface DAOimpl<T> {
    T get(int id) throws SQLException;
   ArrayList <T> getAll() throws SQLException;
}
