package dao;

import lab_start.Cinema;
import lab_start.Film;
import lab_start.Hall;
import lab_start.Session;

import java.sql.*;
import java.util.ArrayList;

public class Dao {
    public Statement stmt;

    public  Dao() throws SQLException {
        Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "444634ar");
         stmt = con.createStatement();

    }
    public class DaoCinema implements DAOimpl<Cinema> {
        public ResultSet rs;
        public Cinema get(int id) throws SQLException {
            Cinema list = new Cinema();
            this.rs = stmt.executeQuery("SELECT * FROM Cinema where id=" + id);
            while (rs.next()) {
                String name = this.rs.getString("name");
                String phoneNumber = this.rs.getString("phone");
                String adress = this.rs.getString("adress");
                list.adress = adress;
                list.name = name;
                list.phoneNumber = phoneNumber;
                list.id = id;
                list.printString();
            }
            rs.close();
            return list;
        }
        public ArrayList<Cinema> getAll() throws SQLException {
            ArrayList<Cinema> list = new ArrayList<>();
            this.rs = stmt.executeQuery("SELECT * FROM Cinema");
            while (this.rs.next()) {
                String name = this.rs.getString("name");
                Integer id = this.rs.getInt("id");
                String phoneNumber = this.rs.getString("phone");
                String adress = this.rs.getString("adress");
                Cinema objCinema = new Cinema(id, name, phoneNumber, adress);
                objCinema.printString();
                list.add(objCinema);
            }
            this.rs.close();
            return list;
        }
    }
    public class DaoHall implements DAOimpl<Hall> {
        ResultSet rs;
        public Hall get(int id) throws SQLException {
            Hall list = new Hall();
            this.rs = stmt.executeQuery("SELECT * FROM Holl where id=" + id);
            while (rs.next()) {
                Integer cinema_id = this.rs.getInt("cinema_id");
                list.id = id;
                list.cinema_id = cinema_id;
                list.printString();
            }
            rs.close();
            return list;
        }
        public ArrayList<Hall> getAll() throws SQLException {
            ArrayList<Hall> list = new ArrayList<>();
            this.rs = stmt.executeQuery("SELECT * FROM Holl");
            while (this.rs.next()) {
                Integer id = this.rs.getInt("id");
                Integer cinema_id = this.rs.getInt("cinema_id");
                Hall objHall = new Hall(id,cinema_id);
                objHall.printString();
                list.add(objHall);
            }
            this.rs.close();
            return list;
        }
    }
    public class DaoFilm implements DAOimpl<Film> {
        ResultSet rs;
        public Film get(int id) throws SQLException {
            Film list = new Film();
            this.rs = stmt.executeQuery("SELECT * FROM Film where id=" + id);
            while (rs.next()) {
                String genre = this.rs.getString("genre");
                String name = this.rs.getString("name");
                String time = this.rs.getString("time");
                Date date = this.rs.getDate("date");
                list.id = id;
                list.genre = genre;
                list.name = name;
                list.time = time;
                list.date = date;
                list.printString();
            }
            rs.close();
            return list;
        }
        public ArrayList<Film> getAll() throws SQLException {
            ArrayList<Film> list = new ArrayList<>();
            this.rs = stmt.executeQuery("SELECT * FROM Film");
            while (this.rs.next()) {
                String genre = this.rs.getString("genre");
                String name = this.rs.getString("name");
                String time = this.rs.getString("time");
                Date date = this.rs.getDate("date");
                Integer id = this.rs.getInt("id");
                Film objFilm = new Film(genre, date, name, time, id);
                objFilm.printString();
                list.add(objFilm);
            }
            this.rs.close();
            return list;
        }
    }
    public class DaoSession implements DAOimpl<Session> {
        ResultSet rs;
        public Session get(int id) throws SQLException {
            Session list = new Session();
            this.rs = stmt.executeQuery("SELECT * FROM Sessions where id=" + id);
            while (rs.next()) {
                String time = this.rs.getString("time");
                Integer freePlace = this.rs.getInt("freeseats");
                Integer film_id = this.rs.getInt("film_id");
                Integer hols_id = this.rs.getInt("hols_id");
                list.id = id;
                list.film_id = film_id;
                list.freePlace = freePlace;
                list.time = time;
                list.hols_id = hols_id;
                list.printString();
            }
            rs.close();
            return list;
        }
        public ArrayList<Session> getAll() throws SQLException {
            ArrayList<Session> list = new ArrayList<>();
            this.rs = stmt.executeQuery("SELECT * FROM Sessions");
            while (this.rs.next()) {
                String time = this.rs.getString("time");
                Integer freePlace = this.rs.getInt("freeseats");
                Integer film_id = this.rs.getInt("film_id");
                Integer hols_id = this.rs.getInt("hols_id");
                Integer id = this.rs.getInt("id");
                Session objSession = new Session(id, film_id, hols_id, time, freePlace);
                objSession.printString();
                list.add(objSession);
            }
            this.rs.close();
            return list;
        }
    }
}
