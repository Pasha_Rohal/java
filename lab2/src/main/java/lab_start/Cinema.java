package lab_start;

public class Cinema {
    public Integer id;
    public String name;
    public String phoneNumber;
    public String adress;

    public Cinema(Integer id, String name, String phoneNumber, String adress){
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.adress = adress;
    }
    public Cinema(){
    }

    public void printString(){
        System.out.println(this.id);
        System.out.println(this.name);
        System.out.println(this.phoneNumber);
        System.out.println(this.adress);
    }

    public Object add(Object obj) {
        return  obj;
    }
}
