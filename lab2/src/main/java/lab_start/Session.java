package lab_start;

public class Session extends Hall {
    public Integer id;
    public String time;
    public Integer freePlace;
    public Integer film_id;
    public Integer hols_id;


    public Session(){}

    public Session(Integer id, Integer film_id,  Integer hols_id, String time, Integer freePlace){
        this.freePlace =freePlace;
        this.id = id;
        this.film_id = film_id;
        this.hols_id = hols_id;
        this.time = time;
    }


    public void printString(){
        System.out.println(id);
        System.out.println(time);
        System.out.println( freePlace);
        System.out.println( film_id);
        System.out.println( hols_id);
    }
}
