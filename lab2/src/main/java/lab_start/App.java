package lab_start;



import dao.Dao;

import java.sql.SQLException;


public class App 
{
    public static void main(String[] args ) throws SQLException {

        Dao dao = new Dao();
        Dao.DaoCinema daoCinema = dao. new DaoCinema();
        Dao.DaoFilm daoFilm = dao. new DaoFilm();
        Dao.DaoHall daoHall = dao. new DaoHall();
        Dao.DaoSession daoSession = dao. new DaoSession();
        System.out.println("<---------->");
        daoCinema.get(2);
        System.out.println("<---------->");
        daoFilm.get(2);
        System.out.println("<---------->");
        daoHall.get(2);
        System.out.println("<---------->");
        daoSession.get(2);
        System.out.println("<---------->");
        daoCinema.getAll();
        System.out.println("<---------->");
    }
}