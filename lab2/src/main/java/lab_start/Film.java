package lab_start;

import java.sql.Date;

public class Film {
    public String genre;
    public String name;
    public Integer id;
    public String time;
    public Date date;

    public Film(){ }

    public Film(String genre, Date date, String name, String time, Integer id){
        this.date = date;
        this.id = id;
        this.name = name;
        this.time = time;
        this.genre = genre;
    }

    public void createNameTitle(String titleName){
        System.out.println(titleName);
    }

    public void changeTimeFilm(String newTime){
        this.time = newTime;
        System.out.println(this.time);
    }

    public void printString(){
        System.out.println(this.id);
        System.out.println(this.name);
        System.out.println(this.genre);
        System.out.println(this.time);
        System.out.println(this.date);
    }
}
