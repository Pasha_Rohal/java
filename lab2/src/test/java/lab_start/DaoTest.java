package lab_start;


import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import lab_start.Cinema;
import lab_start.Film;
import lab_start.Hall;
import lab_start.Session;
import org.junit.Test;
import dao.Dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DaoTest {
    @Test
    public void test1() throws SQLException {
        ArrayList<Cinema> cinemaTest1 = new ArrayList<>();
        for(int i = 0; i < 3; i++){
            Cinema c = new Cinema(i,"t"+i,""+i,"a"+i);
            cinemaTest1.add(c);
        }
        ArrayList<Cinema> cinemaTest2 = new ArrayList<>(cinemaTest1);
        Dao.DaoCinema daoCinema = mock(Dao.DaoCinema.class);
        when(daoCinema.getAll()).thenReturn(cinemaTest1);
        ArrayList<Cinema> allCinema = daoCinema.getAll();

        assertEquals(allCinema,cinemaTest2);
        assertEquals(allCinema.size(),3);
        for(int i = 0; i < 3; i++){
            assertEquals(allCinema.get(i).id, new Integer(i));
            assertEquals(allCinema.get(i).adress,"a"+i);
            assertEquals(allCinema.get(i).phoneNumber, ""+i);
            assertEquals(allCinema.get(i).name,"t"+i);
        }
    }
    @Test
    public void test2() throws SQLException {
        Cinema cinemaTest1 = new Cinema(1,"2","2","2");
        Dao.DaoCinema daoCinema = mock(Dao.DaoCinema.class);
        when(daoCinema.get(1)).thenReturn(cinemaTest1);
        Cinema c = daoCinema.get(1);
        assertEquals(c.name, "2");
        assertEquals(c.id, new Integer(1));
        assertEquals(c.adress, "2");
        assertEquals(c.phoneNumber, "2");
    }

    @Test
    public void test3() throws SQLException {
        ArrayList<Film> filmTest1 = new ArrayList<>();
        for(int i = 0; i < 3; i++){
            Film f = new Film("g"+i,new Date(i), "n"+i, i+":"+i, new Integer(i));
            filmTest1.add(f);
        }
        ArrayList<Film> filmTest2 = new ArrayList<>(filmTest1);
        Dao.DaoFilm daoFilm = mock(Dao.DaoFilm.class);
        when(daoFilm.getAll()).thenReturn(filmTest1);
        ArrayList<Film> allFilm = daoFilm.getAll();

        assertEquals(allFilm,filmTest2);
        assertEquals(allFilm.size(),3);
        for(int i = 0; i < 3; i++){
            assertEquals(allFilm.get(i).id, new Integer(i));
            assertEquals(allFilm.get(i).genre,"g"+i);
            assertEquals(allFilm.get(i).date, new Date(i));
            assertEquals(allFilm.get(i).time, i+":"+i);
            assertEquals(allFilm.get(i).name,"n"+i);
        }
    }

    @Test
    public void test4() throws SQLException {
        Film filmTest1 = new Film("g",new Date(1), "n", "1:1", new Integer(1));
        Dao.DaoFilm daoFilm = mock(Dao.DaoFilm.class);
        when(daoFilm.get(1)).thenReturn(filmTest1);
        Film f = daoFilm.get(1);
        assertEquals(f.id, new Integer(1));
        assertEquals((f).genre,"g");
        assertEquals(f.date, new Date(1));
        assertEquals(f.time, "1:1");
        assertEquals(f.name,"n");
    }

    @Test
    public void test5() throws SQLException {
        ArrayList<Hall> hallTest1 = new ArrayList<>();
        for(int i = 0; i < 3; i++){
            Hall h = new Hall(i,i);
            hallTest1.add(h);
        }
        ArrayList<Hall> hallTest2 = new ArrayList<>(hallTest1);
        Dao.DaoHall daoHall = mock(Dao.DaoHall.class);
        when(daoHall.getAll()).thenReturn(hallTest1);
        ArrayList<Hall> allHall = daoHall.getAll();

        assertEquals(allHall,hallTest2);
        assertEquals(allHall.size(),3);
        for(int i = 0; i < 3; i++){
            assertEquals(allHall.get(i).id, new Integer(i));
            assertEquals(allHall.get(i).cinema_id,new Integer(i));
        }
    }

    @Test
    public void test6() throws SQLException {
        Hall hallTest1 = new Hall(1,1);
        Dao.DaoHall daoHall = mock(Dao.DaoHall.class);
        when(daoHall.get(1)).thenReturn(hallTest1);
        Hall h = daoHall.get(1);
        assertEquals(h.id, new Integer(1));
        assertEquals((
                h).cinema_id,new Integer(1));
        assertEquals(h.id, new
        Integer(1));
    }

    @Test
    public void test7() throws SQLException {
        ArrayList<Session> sessionTest1 = new ArrayList<>();
        for(int i = 0; i < 3; i++){
            Session s = new Session(i,i,i, i+":"+i, i);
            sessionTest1.add(s);
        }
        ArrayList<Session> sessionTest2 = new ArrayList<>(sessionTest1);
        Dao.DaoSession daoSession = mock(Dao.DaoSession.class);
        when(daoSession.getAll()).thenReturn(sessionTest1);
        ArrayList<Session> allSession = daoSession.getAll();

        assertEquals(allSession,sessionTest2);
        assertEquals(allSession.size(),3);
        for(int i = 0; i < 3; i++){
            assertEquals(allSession.get(i).id, new Integer(i));
            assertEquals(allSession.get(i).film_id,new Integer(i));
            assertEquals(allSession.get(i).freePlace, new Integer(i));
            assertEquals(allSession.get(i).hols_id,new Integer(i));
            assertEquals(allSession.get(i).time, i+":"+i);
        }
    }
    @Test
    public void test8() throws SQLException {
        Session sessionTest1 = new Session(1,1,1, "1:1", 1);
        Dao.DaoSession daoSession = mock(Dao.DaoSession.class);
        when(daoSession.get(1)).thenReturn(sessionTest1);
        Session s = daoSession.get(1);
        assertEquals(s.id, new Integer(1));
        assertEquals(s.film_id,new Integer(1));
        assertEquals(s.freePlace, new Integer(1));
        assertEquals(s.hols_id,new Integer(1));
        assertEquals(s.time, "1:1");
    }
}