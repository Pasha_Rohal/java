package lab_start;

import org.junit.*;

import java.sql.Date;


public class FilmTest {
    Film film = new Film("11/02/2009", new Date(1),"Anime","1:31",1);
    Session session = new Session();

    public void setSession(Session session) {
        this.session = session;
        this.session.id = 100;
        film.id = this.session.id;
    }
    @Test
    public void shouldAnswerWithEqual()
    {
        setSession(session);
        Assert.assertEquals("id equal 1",(Integer) 100, film.id);
    }
}
