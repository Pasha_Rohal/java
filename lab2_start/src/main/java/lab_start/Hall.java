package lab_start;

public class Hall {
    public  Integer count;
    public  Integer id;

    public Hall(Integer id, Integer count){
        this.id = id;
        this.count = count;
    }
    public Hall(){}

    public void printCount(){
        System.out.println(this.count);
    }

    public Integer countChange(Integer newCount){
        this.count = newCount;
        System.out.println(this.count);
        return this.count;
    }
}
