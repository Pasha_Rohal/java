package lab_start;

public class Film {
    public String name;
    public Integer id;
    public String time;
    public String date;

    public Film(){ }

    public Film(String date, String name, String time, Integer id){
        this.date = date;
        this.id = id;
        this.name = name;
        this.time = time;
    }

    public void createNameTitle(String titleName){
        System.out.println(titleName);
    }

    public void changeTimeFilm(String newTime){
        this.time = newTime;
        System.out.println(this.time);
    }

    public Object createDuplicateFilm(Object object){
        return object;
    }
}
