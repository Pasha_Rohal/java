package lab_start;

public class A {

    private  int foo;
    private int bar;


    void incBar(){
        bar++;
    }

   static public  class B {
        private A outer;

        public B(A outer){
            this.outer = outer;
        }

        private  int b1;
        private int b2;

        void m1(){
            b1 = outer.foo;
            outer.incBar();
        }
    }
}
