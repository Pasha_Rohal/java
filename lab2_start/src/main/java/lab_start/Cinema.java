package lab_start;

public class Cinema {
    public Integer id;
    public String name;
    public String phoneNumber;
    public Cinema(Integer id, String name, String phoneNumber){
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }
    public Cinema(){
    }
    public Object getById(Integer id){
        return id;
    }
    public Object add(Object obj){
        return obj;
    }
}
