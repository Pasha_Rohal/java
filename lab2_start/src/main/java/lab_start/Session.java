package lab_start;

public class Session extends Hall {
    public Integer id;
    public String name;
    public String time;
    public Integer freePlace;

    public Session(){}

    public Session( Integer id, String name, String time, Integer freePlace){
        super(1,100);
        this.freePlace =freePlace;
        this.id = id;
        this.name = name;
        this.time = time;
    }

    public int takeAllPlaces(){
        return count;
    }

    public int occupiedPlaces(){
        int occPlaces = count - this.freePlace;
        return occPlaces;
    }
}
