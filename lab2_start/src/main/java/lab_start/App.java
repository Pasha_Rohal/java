package lab_start;



public class App 
{
    static class Anal {
        @Override
        public String toString() {
            return "Vanya Krasava";
        }
    }

    public static void main( String[] args )
    {
        A a = new A();
        A.B b1= new A.B(a);

        b1.m1();

        Object obj = new Object() {
            @Override
            public String toString() {
                return "Vanya Krasava";
            }
        };
        Object obj1 = new Anal();
        System.out.println(obj1.toString());
    }
}
