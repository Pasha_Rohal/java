package lab_start;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CinemaTest {
    Cinema cinema = new Cinema(1,"Multiplex","+380660995770");

    @Test
    public void shouldAnswerWithEqualID()
    {
        Assert.assertEquals("id equal 1",(Integer) 1,cinema.id);
    }

    @Test
    public void shouldAnswerWithEqualName()
    {
        Assert.assertEquals("name is Multiplex","Multiplex",cinema.name);
    }

    @Test
    public void shouldAnswerWithEqualPhoneNumber()
    {
        Assert.assertEquals("Phone number equal +380660995770","+380660995770",cinema.phoneNumber);
    }
    @Test
    public void shouldAnswerWithEqualAdd()
    {
        Object obj = 1;

        Assert.assertEquals("Add Method Test",1,cinema.add(obj));
    }
}
