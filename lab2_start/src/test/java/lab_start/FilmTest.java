package lab_start;

import org.junit.Assert;
import org.junit.Test;

public class FilmTest {
    Film film = new Film("11/02/2009","Anime","1:31",1);
    Session session = new Session();

    public void setSession(Session session) {
        this.session = session;
        this.session.count = 100;
        film.id = this.session.count;
    }
    @Test
    public void shouldAnswerWithEqual()
    {
        setSession(session);
        Assert.assertEquals("id equal 1",(Integer) 100, film.id);
    }
}
