package lab_start;

import org.junit.Assert;
import org.junit.Test;

public class SessionClass {
    Session session = new Session(1,"FirstSession","2 hour", 70);

    @Test
    public void shouldAnswerWithEqualCount()
    {
        Assert.assertEquals("Check not freee Place",30,session.occupiedPlaces());
    }
    @Test
    public void shouldAnswerWithEqualAllPlaces()
    {
        Assert.assertEquals("Check  Places",100,session.takeAllPlaces());
    }
}
